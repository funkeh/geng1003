/*
 * Author: Josef Frank  StNo: 3162099   Lab: Wed 2PM
 * Date: 19-Aug-2015
 *
 * Calculating the monthly repayments of a loan.
 */

#include <stdio.h>
#include <math.h>

/* Constants */
#define MONTHSINYEAR 12

/* Function protoypes */
double monthlypayment(double principal, double rate, int term);

int main()
{

  double payment,   /* Montly payment required in dollars */
         principal, /* Total amount borrowed in dollars */
         rate;      /* Annual interest rate */
  int term;         /* Length of loan in years */

  /* Acquire info */
  printf("What is the total amount borrowed (dollars)? ");
  scanf("%lf", &principal);
  printf("What is the annual interest rate? ");
  scanf("%lf", &rate);
  printf("What is the length of the loan (years)? ");
  scanf("%i", &term);

  payment = monthlypayment(principal, rate, term);

  printf("The montly repayment for the loan is %.2lf.\n", payment);

  return 0;
}


/*
 * This function calculates the monthly payment on a loan with a given
 * principal, rate, and term.
 * INPUTS: principal (double, Amount borrowed in dollars)
 *         rate (double, Interest rate [Annual])
 *         term (int, The full length of the loan in years)
 * OUTPUT: Calculated monthly payment in dollars (double)
 */
double monthlypayment(double principal, double rate, int term) {

  /* Convert length of term from years into months */
  term = term * MONTHSINYEAR;

  /* Calculate rate from annual to monthly */
  rate = rate / (MONTHSINYEAR * 100);

  /* Calculate and return monthly payment */
  return ((rate * principal) / (1 - pow(1 + rate, -term)));

}
