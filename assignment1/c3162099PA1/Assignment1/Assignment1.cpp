/*
 * Author: Josef Frank  StNo: 3162099   Lab: Wed 2PM
 * Date: 8-Sept-2015
 *
 * Assignment 1
 */

#include "stdafx.h"
#include <stdlib.h>
#include <math.h>

#define FILE_NAME "input1.dat"

/* Function prototypes */
double calc_relative_error(double, double);
double calc_velocity(double, double, double, double, double);
double calc_trapezium_area(double, double, double, double,
						   double, int, int, double);


int main(void) {

	bool different_side,		/* xi and xf on diff sides of singularity */
		 equals_singularity;	/* xi or xf equal singularity */

	double a, b, c, d,		/* Values to be used in calc_velocity */
		   area_1, area_2,	/* Initial and current area values */
		   h,				/* The step size for the trapezoidal function */
		   relative_error,	/* Stores the calculated error value */
	       xi, xf;			/* Initial and final x values */

	int counter,		/* Keeps track of the trapezoidal calculation loop */
		error_code,		/* Error code */
		n,				/* Number of steps to be taken in trapezoidal calc */
		row;			/* Row which we are currently reading */

	FILE *input;	/* File reader */


	/* Open file and collect error if unable to */
	error_code = fopen_s(&input, FILE_NAME, "r");

	/* If error is prevalent */
	if (error_code > 0) {

		/* Print error message */
		printf("Failed to open \"%s\". Program terminating.\n", FILE_NAME);

		/* End program */
		return EXIT_FAILURE;

	}


	/* Start at row 1, increase each while step for use in error reporting */
	row = 1;

	/* Interpret new lines until out of new lines */
	while (fscanf_s(input, "%lf%lf%lf%lf%lf%lf",
							&a, &b, &c, &d, &xi, &xf) != EOF) {

		/* Calculate whether xi and xf are on different sides of singularity */
		different_side = ((xi < 1 && xf > 1) || (xi < -1 && xf > -1));

		/* Calculate whether either xi or xf equal a singularity */
		equals_singularity = (abs(xi) == 1 || abs(xf) == 1);

		/* Check if the data contains either error */
		if (different_side || equals_singularity) {

			/* Print an error message */
			printf("%2d: Row contains a singularity.\n", row);

			/* Move to the next iteration of the loop*/
			continue;

		}


		/* Print row data */
		printf("%2d: %2.1lf  %2.1lf  %2.1lf  %2.1lf  %2.1lf  %2.1lf\n",
			row, a, b, c, d, xi, xf);

		/* Initialise variables */
		area_1 = 0;
		relative_error = 1;

		/* Repeat until relative error small or counter reaches 6 */
		for (counter = 1; relative_error > 0.00001 && counter < 6; counter++) {

			/* Calculate step size */
			h = pow(10.0, -counter);

			/* Calculate number of steps */
			n = (int)((abs(xf - xi)) / h);

			/* Calculate current area */
			area_2 = calc_trapezium_area(a, b, c, d, xi, counter, n, h);

			/* Print calculated values */
			printf("    h: %9.7f       N: %7d      area: %6.6lf\n",
					h, n, area_2);

			/* Calculate relative error */
			relative_error = calc_relative_error(area_1, area_2);

			/* Store current area in previous area variable */
			area_1 = area_2;

		}

		/* Increase row counter */
		row++;

	}

	return EXIT_SUCCESS;

}


/*
* Calculates the relative error.
* INPUT:  area_1, area_2 -- all double
* OUTPUT: Relative error of two areas
*/
double calc_relative_error(double area_1, double area_2) {

	return abs((area_2 - area_1) / area_2);

}


/*
* Calculates the area of a given trapezium.
* INPUT:  a, b, c, d, xi, h -- double
*         counter, n -- int
* OUTPUT: Area of a given trapezium
*/
double calc_trapezium_area(double a, double b, double c, double d,
	double xi, int counter, int n, double h) {

	double step,	/* Step of area loop */
		   sum = 0; /* Sum of all calculated areas */

	/* Repeat for counter number of times */
	for (step = 0; step < counter; step++) {

		/* Calculate area and add it to the sum */
		sum += (h * (calc_velocity(a, b, c, d, xi) +
			         calc_velocity(a, b, c, d, xi + h)) / 2.0);

		/* Increase xi by step of h */
		xi += h;

	}

	return sum;

}


/*
 * Calculates the velocity of a given function.
 * INPUT:  a, b, c, d, x -- all double
 * OUTPUT: Velocity value for the given x value
 */
double calc_velocity(double a, double b, double c, double d, double x) {

	return abs(((a * x * x * x) + (b * x * x) + (c * x) + d) /
		       ((x + 1) * (x - 1)));

}
