/* GENG1003
 * Josef Frank, c3162099
 * Hello World!
 */

#include <stdio.h>
#include <string.h>

int main(void) {

  /* Name char array */
  char name[16];

  double x = 7.89898;

  printf("%f", x);

  while(strcmp("exit", name) != 0) {

    /* Get player's name */
    printf("What is your name? ");
    scanf("%s", &name);

    /* Say hello */
    printf("Hi %s!\n", name);

  }

  /* Terminate program with successful exit status */
  return 0;

}
