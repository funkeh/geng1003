/*
 * Author: Josef Frank  StNo: 3162099   Lab: Wed 2PM
 * Date: 8-Oct-2015
 *
 * Assignment 2
 */

#include "stdafx.h"
#include <stdlib.h>
#include <string.h>

/* Defaults for team struct */
#define DEFAULT_GAMES_PLAYED 0
#define DEFAULT_GOALS_AGAINST 0
#define DEFAULT_GOALS_FOR 0
#define DEFAULT_LEAGUE_POINTS 0

/* Miscellaneous constants */
#define ERROR_LIMIT 0
#define FILE_NAME_A "input2a.dat"
#define FILE_NAME_B "input2b.dat"
#define MAX_NAME_LENGTH 16
#define NOT_FOUND -1
#define NUM_TEAMS 20


/* Structs */
typedef struct {

    char    name[MAX_NAME_LENGTH];
    int     games_played;
    int     goals_for;
    int     goals_against;
    int     league_points;
} team_t;


/* Function prototypes */
int  getGoalDiff    (team_t* teams, int id);
int  getTeamLocation(team_t* teams, char name[MAX_NAME_LENGTH]);
void printTeamData  (team_t* teams);
void sortData       (team_t* teams);
void swapData       (team_t* teams, int id_a, int id_b);


int main(void) {

    bool loop_ended;    /* Whether loop continues */

    char error_text[20],            /* Error collected */
         name_a[MAX_NAME_LENGTH],   /* Name of team A */
         name_b[MAX_NAME_LENGTH];   /* Name of team B */

    FILE *input_a,      /* Contains team names */
         *input_b;      /* Contains match scores */

    int error_code_a,   /* Error code obtained from reading file A */
        error_code_b,   /* Error code obtained from reading file B */
        error_code_c,   /* Error code obtained from scanning from file B */
        id_a, id_b,     /* Location of teams with given name */
        score_a,        /* Goals scored by team A */
        score_b;        /* Goals scored by team B*/

    team_t teams[NUM_TEAMS];    /* Array of all teams */


    /* Open files and collect error messages */
	error_code_a = fopen_s(&input_a, FILE_NAME_A, "r");
	error_code_b = fopen_s(&input_b, FILE_NAME_B, "r");

    /* If error in opening either file... */
	if (error_code_a > ERROR_LIMIT || error_code_b > ERROR_LIMIT) {

        /* Report error and end program */
		printf("Failed to read data files.\n");
		return EXIT_FAILURE;
	}


    /* Load all team data from first file */
    for (int i = 0; i < NUM_TEAMS; i++) {

        /* Read team name from file */
        fscanf_s(input_a, "%s", teams[i].name, MAX_NAME_LENGTH);

        /* Initialise team variables */
        teams[i].games_played  = DEFAULT_GAMES_PLAYED;
        teams[i].goals_against = DEFAULT_GOALS_AGAINST;
        teams[i].goals_for     = DEFAULT_GOALS_FOR;
        teams[i].league_points = DEFAULT_LEAGUE_POINTS;
    }

    /* Close first file */
    fclose(input_a);


    loop_ended = false;     /* Loop has not yet ended */

    do {

        /* If no name found, collect EOF */
        error_code_c = fscanf_s(input_b, "%s", name_a, MAX_NAME_LENGTH);
        fscanf_s(input_b, "%d", &score_a);
        fscanf_s(input_b, "%s", name_b, MAX_NAME_LENGTH);
        fscanf_s(input_b, "%d", &score_b);

        /* If first value scanned is an end of file, end loop */
        if (error_code_c == EOF) {

            loop_ended = true;
        }
        else {

            /* Discover locations of teams */
            id_a = getTeamLocation(teams, name_a);
            id_b = getTeamLocation(teams, name_b);

            /* Check for errors */
            if (id_a == NOT_FOUND || id_b == NOT_FOUND) {

                /* Print error message */
                printf("%20s", "Team doesn't exist");
            }
            else if (score_a < 0 || score_b < 0) {

                /* Set error message */
                printf("%20s", "Score out of bounds");
            }
            else {

                /*  */
                printf("%20s", "Data valid");

                /* Increase number of games played for each team */
                teams[id_a].games_played++;
                teams[id_b].games_played++;

                /* Record goal stats for team a */
                teams[id_a].goals_for += score_a;
                teams[id_a].goals_against += score_b;

                /* Record goal stats for team b*/
                teams[id_b].goals_for += score_b;
                teams[id_b].goals_against += score_a;

                /* Increase team A's score if winning */
                if (score_a > score_b) {

                    teams[id_a].league_points += 3;
                }
                /* Increase team B's score if winning */
                else if (score_a < score_b) {

                    teams[id_b].league_points += 3;
                }
                /* Increase both teams' score if draw */
                else {

                    teams[id_a].league_points += 1;
                    teams[id_b].league_points += 1;
                }
            }

            /* Print row of data */
            printf(": %16s: %4d %16s: %4d\n",
                name_a, score_a,
                name_b, score_b);
        }
    } while (!loop_ended);


    /* Print alphabetical output */
    printf("\nTeam Results - Alphabetical Order\n\n");
    printTeamData(teams);

    /* Sort team data by performance */
    sortData(teams);

    /* Print performance ranking output */
    printf("\nTeam Results - Performance Ranking\n\n");
    printTeamData(teams);


	return EXIT_SUCCESS;
}


/*
 * PURPOSE: Calculate goal difference of a team.
 * INPUT: Array of team_t variables, and id of team in array.
 * OUTPUT: Calculated goal difference.
 */
int getGoalDiff(team_t* teams, int id) {

    return teams[id].goals_for - teams[id].goals_against;
}


/*
 * PURPOSE: Find location of value in array.
 * INPUT: Array of team_t variables, and name to search for.
 * OUTPUT: Location of value in array, or -1 if not found.
 */
int getTeamLocation(team_t* teams, char name[MAX_NAME_LENGTH]) {

    /* Iterate through all team_t variables */
    for (int i = 0; i < NUM_TEAMS; i++) {

        /* Return true if team name is found */
        if (strcmp(teams[i].name, name) == 0) {

            return i;
        }
    }

    return NOT_FOUND;   /* Return false is name not found */
}


/*
 * PURPOSE: To print all team data in a table format.
 * INPUT: Array of team_t variables.
 * OUTPUT: Nothing.
 */
void printTeamData(team_t* teams) {

    /* Print header of data table */
    printf("%16s | %6s | %6s | %9s | %11s\n",
        "Name",
        "Played",
        "Points",
        "Goal-Diff",
        "Goal-Scored");

    /* Print row of hyphens */
    for (int i = 0; i < 60; i++)
        printf("-");

    /* Print a newline after hyphens */
    printf("\n");

    /* Iterate through all variables in array */
    for (int i = 0; i < NUM_TEAMS; i++) {

        /* Print row of data for each variable */
        printf("%16s | %6d | %6d | %9d | %11d\n",
            teams[i].name,
            teams[i].games_played,
            teams[i].league_points,
            getGoalDiff(teams, i),
            teams[i].goals_for);
    }
}


/*
 * PURPOSE: Sort data in array by performance ranking.
 * INPUT: Array of teams.
 * OUTPUT: Nothing.
 */
void sortData(team_t* teams) {

    /* Stores whether data was swapped this iteration */
    bool data_swapped;

    /* Stores calculated  */
    int goal_diff_1, goal_diff_2;

    /* Repeat until data was not been swapped */
    do {

        /* Reset data swapped value */
        data_swapped = false;

        /* Iterate through all teams */
        for (int i = 0; i < (NUM_TEAMS - 1); i++) {

            /* If team B has more league points, swap */
            if (teams[i].league_points < teams[i + 1].league_points) {

                data_swapped = true;
                swapData(teams, i, i + 1);
            }
            /* If team A and team B have the same points, continue */
            else if (teams[i].league_points == teams[i + 1].league_points) {

                /* Calculate goal differences */
                goal_diff_1 = getGoalDiff(teams, i);
                goal_diff_2 = getGoalDiff(teams, i + 1);

                /* If team B has higher goal difference than team A, swap */
                if (goal_diff_1 < goal_diff_2) {

                    data_swapped = true;
                    swapData(teams, i, i + 1);
                }
                /* If same goal differences, continue */
                else if (goal_diff_1 == goal_diff_2) {

                    /* If team B has scored more goals than A, swap */
                    if (teams[i].goals_for < teams[i + 1].goals_for) {

                        data_swapped = true;
                        swapData(teams, i, i + 1);
                    }
                    /* If team A has the same number of goals as B, continue */
                    else if (teams[i].goals_for == teams[i + 1].goals_for) {

                        /* Alphabetical sort if needed */
                        if (strcmp(teams[i].name, teams[i + 1].name) > 0) {

                            data_swapped = true;
                            swapData(teams, i, i + 1);
                        }
                    }
                }
            }
        }
    } while (data_swapped);
}


/*
 * PURPOSE: Swap data in two positions of an array.
 * INPUT: Teams array, both positions.
 * OUTPUT: Nothing.
 */
void swapData(team_t* teams, int id_a, int id_b) {

    /* Store team A in temporary variable */
    team_t tmp = teams[id_a];

    /* Actually swap variables */
    teams[id_a] = teams[id_b];
    teams[id_b] = tmp;

}
