/*
 * Author: Josef Frank  StNo: 3162099   Lab: Wed 2PM
 * Date: 15-Sept-2015
 *
 * Lab 7 Preparation
 */

#include "stdafx.h"
#include <math.h>
#include <stdlib.h>


/* Constants */
#define ARRAY_SIZE 20
#define FILE_NAME "input.dat"
#define SENTINEL_VALUE -999


/* Prototypes */
void process_z(double x[], double y[], int size);


int main(void) {

	/* True if reading data into array x rather than y */
	bool reading_x = true;

	double value,			/* Value read from file */
		   x[ARRAY_SIZE],	/* Arrays for    */
		   y[ARRAY_SIZE];	/* storing input */

	int current_dataset = 1,	/* Current data set being read */
		error_code,				/* Error code obtained from reading file */
		i = 0,					/* Counter of coming loops */
		length_x, length_y;		/* Number of data pieces read from dataset */

	/* Input stream */
	FILE* input;


	/* Open file while collecting errors */
	error_code = fopen_s(&input, FILE_NAME, "r");

	if (error_code != 0) {

		/* Print error message */
		printf("Failed to open \"%s\". Program terminating.\n", FILE_NAME);

		/* End program with error */
		return EXIT_FAILURE;
	}


	/* Scan input until reaching the end of the file */
	while (fscanf_s(input, "%lf", &value) != EOF) {

		if (value == SENTINEL_VALUE) {

			if (reading_x) {

				/* Set length of x to the number of elements from this cycle */
				length_x = i;
			}
			else {

				/* Set length of y to the number of elements from this cycle */
				length_y = i;

				/* If recorded datasets are different lengths */
				if (length_x != length_y) {

					/* Report error */
					printf("Lists in dataset %i are different lengths.\n\n",
							current_dataset);
				}
				else {

					/* Calculate and print products of x and y and square root of z sum */
					process_z(x, y, length_x);
				}

				/* Increase number indicating the current dataset */
				current_dataset++;
			}

			/* Invert whether we're reading x or not */
			reading_x = !reading_x;

			/* Reset counter */
			i = 0;
		}
		else {

			/* Interpret elements as long as we haven't exceeded array size */
			if (i < ARRAY_SIZE) {

				if (reading_x) {

					/* Add current value to x */
					x[i] = value;
				}
				else {

					/* Add current value to y */
					y[i] = value;
				}

				/* Increment counter */
				i++;
			}
		}
	}


	/* Quit with success message */
	return EXIT_SUCCESS;

}


/*
 * Processes two arrays, prints the product and the square root of the sum.
 * INPUT: x, y -- double arrays
 *		  size -- int, size of both arrays
 * OUTPUT: Nothing
 */
void process_z(double x[], double y[], int size) {

	double sum = 0,			/* Cumulative value of z array */
		   z[ARRAY_SIZE];	/* Stores product of x[i] and y[i] */

	/* Counter for loops */
	int i;

	/* Print header */
	printf("   x         y         z\n");

	/* Print divider bar and newline */
	for (i = 0; i < 26; i++) { printf("-"); }
	printf("\n");

	/* Iterate through all values of x and y */
	for (i = 0; i < size; i++) {

		/* Calculate the product of x and y and store in z */
		z[i] = x[i] * y[i];

		/* Collect the full sum of z */
		sum += z[i];

		/* Print a row of properly formatted data */
		printf("%6.2lf    %6.2lf    %6.2lf\n", x[i], y[i], z[i]);
	}

	/* Inform the user of the square root of the sum of z */
	printf("\nSquare root of the sum of the items in z is %lf\n\n", sqrt(sum));
}
