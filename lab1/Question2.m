% Josef Frank :: c3162099
% Question 2

P = rand(2, 10);
X = P(1, :);
Y = P(2, :);

scatter(X, Y, 'o');
hold on

for i = [pi / 64, pi / 32, pi / 16, pi / 8, pi / 4]
    
    % Rotational matrix
    R = [cos(i), sin(i); -sin(i), cos(i)];
    
    Z = R * P;
    X = Z(1, :);
    Y = Z(2, :);
    
    scatter(X, Y, 'x');
end

axis([0, 1, 0, 1]);
hold off;