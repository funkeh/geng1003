/*
 * Author: Josef Frank  StNo: 3162099   Lab: Wed 2PM
 * Date: 12-Aug-2015
 *
 * Calculating the acceleration (m/s squared) of a jet fighter.
 */

#include <stdio.h>

#define GRAVITY 9.80665
#define KPHTOMPS 3.6

int main()
{

  double distance, speed, time, acceleration;

  printf("What distance is available for takeoff (metres)? ");
  scanf("%lf", &distance);
  printf("What speed is required for takeoff (km/h)? ");
  scanf("%lf", &speed);

  /* Convert to metres per second from kilometres per hour */
  speed = speed / KPHTOMPS;

  time = (2 * distance) / speed;

  acceleration = speed / time;

  printf("The acceleration required in m/s is %.2lf.\n", acceleration);
  printf("The time required in seconds is %.2lf.\n", time);
  printf("The acceleration required in terms of g-force is %.2lf.", acceleration / GRAVITY);

  return 0;
}
